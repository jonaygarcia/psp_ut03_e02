# RMI

## Introducción

__RMI__ (_Java Remote Method Invocation_) es un mecanismo ofrecido por Java para invocar un método de manera remota. Forma parte del entorno estándar de ejecución de Java y proporciona un mecanismo simple para la comunicación de servidores en aplicaciones distribuidas basadas exclusivamente en Java.

> __Nota__: Si se requiere comunicación entre otras tecnologías debe utilizarse _CORBA_ o _SOAP_ en lugar de RMI.

_RMI_ se caracteriza por la facilidad de su uso en la programación por estar específicamente diseñado para Java. Proporciona paso de _objetos por referencia_ (no permitido por SOAP), _recolección de basura distribuida_ (Garbage Collector distribuido) y paso de _tipos arbitrarios_ (funcionalidad no provista por CORBA).

A través de RMI, un programa Java puede exportar un objeto, con lo que dicho objeto estará accesible a través de la red y el programa permanece a la espera de peticiones en un puerto TCP. A partir de ese momento, un cliente puede conectarse e invocar los métodos proporcionados por el objeto.

La invocación se compone de los siguientes pasos:

* _Encapsulado_ (marshalling) de los parámetros (utilizando la funcionalidad de serialización de Java).
* _Invocación del método_ (del cliente sobre el servidor). El invocador se queda esperando una respuesta.
* Al terminar la ejecución, el servidor serializa el valor de retorno (si lo hay) y lo envía al cliente.
* El código cliente recibe la respuesta y continúa como si la invocación hubiera sido local.

Más información sobre RMI en la [documentación oficial de Oracle](https://docs.oracle.com/javase/tutorial/rmi/index.html)


## La Interfaz Remota

Lo primero que debemos diseñar es la __Interfaz Remota__ (_Remote Interface_) que implementarán tanto el Servidor como el Cliente. La interfaz debe ser siempre pública y extender de la clase _Remote_. Todos los métodos descritos en la Interfaz Remota deben definir _RemoteException_ en la cláusula throws.

En este ejemplo, la interfaz _RMIInterface_ sólo tiene un método llamado _helloTo_ recibe un String como parámetro y devuelve otro String. A continuación se muestra el código de la interfaz _RMIInterface_:

```java
// Fichero: RMIInterface.java
package com.psp.jonaygarcia.rmiinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {

    public String helloTo(String name) throws RemoteException;

}
```

## El Servidor RMI

Nuestro servidor extiende de la clase UnicastRemoteObject e implementa la interfaz _RMIInterface_ descrita anteriormente. En el método main, el servidor sirve peticiones en _localhost_ con el nombre _MyServer_.

> __Nota__: Al servir peticiones en localhost, el servidor sólo es accesible desde el equipo donde está sirviendo, es decir, el cliente que se conecte al servidor debe estar en la misma máquina para poder acceder a ella.

A continuación se muestra el código de la clase _ServerOperation_:

```java
// Fichero: ServerOperation.java
package com.psp.jonaygarcia.rmiserver;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.mkyong.rmiinterface.RMIInterface;

public class ServerOperation extends UnicastRemoteObject implements RMIInterface{

    private static final long serialVersionUID = 1L;

    protected ServerOperation() throws RemoteException {

        super();

    }

    @Override
    public String helloTo(String name) throws RemoteException{

        System.err.println(name + " is trying to contact!");
        return "Server says hello to " + name;

    }

    public static void main(String[] args){

        try {

            Naming.rebind("//localhost/MyServer", new ServerOperation());
            System.err.println("Server ready");

        } catch (Exception e) {

            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();

        }

    }

}
```

## El Cliente RMI

Para encontrar al Servidor, el cliente usa un objeto _RMIInterface_ que busca una referencia del objeto remoto asociado con el nombre que le pasamos por parámetro. Con este objeto _RMIInterface_ podemos hablar con el Servidor y recibir respuesta.

```java
// Fichero: ClientOperation.java
package com.psp.jonaygarcia.rmiclient;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

import com.mkyong.rmiinterface.RMIInterface;

public class ClientOperation {

	private static RMIInterface look_up;

	public static void main(String[] args)
		throws MalformedURLException, RemoteException, NotBoundException {

		look_up = (RMIInterface) Naming.lookup("//localhost/MyServer");
		String txt = JOptionPane.showInputDialog("What is your name?");

		String response = look_up.helloTo(txt);
		JOptionPane.showMessageDialog(null, response);

	}

}
```

## Ejecución de la Aplicación

Lo primero que debemos hacer es abrir un terminal y situarnos en el directorio raíz de nuestro proyecto. A continuación. compilaremos el código de nuestra aplicación:

```bash
javac src/com/psp/jonaygarcia/rmiinterface/RMIInterface.java src/com/psp/jonaygarcia/rmiserver/ServerOperation.java src/com/psp/jonaygarcia/rmiclient/ClientOperation.java
```

### Ejecución del Servidor

Para ver que hemos compilado correctamente el proyecto debemos comprobar que ha generado correctamente los ficheros _.class_:

```bash
find . -name *.class
./src/com/psp/jonaygarcia/rmiclient/ClientOperation.class
./src/com/psp/jonaygarcia/rmiinterface/RMIInterface.class
./src/com/psp/jonaygarcia/rmiserver/ServerOperation.class
```

Lo siguiente es arrancar el __rmiregistry__:

```bash
$ cd src

$ rmiregistry -J-Djava.class.path=com.psp.jonaygarcia.rmiinterface.RMIInterface &
[1] 929

$ java com.psp.jonaygarcia.rmiserver.ServerOperation
Server ready
```

Por defecto _RMI_ escucha en el puerto 1099, para comprobar que el servidor está ejecutándose:

```bash
$ netstat -na | grep 1099
tcp46      0      0  *.1099                 *.*                    LISTEN
```

Ahora ejecutamos el Servidor de nuestra aplicación, para ello, abrimos un terminal, y nos colocamos dentro de la carpeta src de nuestro proyecto:

```bash
$ cd src

$ java com.psp.jonaygarcia.rmiserver.ServerOperation
Server ready
```

### Ejecución del Cliente

Ahora ejecutamos el Cliente de nuestra aplicación, para ello, abrimos un terminal, y nos colocamos dentro de la carpeta src de nuestro proyecto:

```bash
$ cd src

$ java com.psp.jonaygarcia.rmiclient.ClientOperation
```

Si todo va bien, debería abrirse la siguiente ventana solicitando escribir un nombre:

![img_01][img_01]

Si escribimos un nombre, por ejemplo _Alice_, y pulsamos el botón _Aceptar_, deberemos ver algo como lo siguiente:

En la terminal del _Cliente_:

```bash
Server says hello to Alice;
```
En el terminal del _Servidor_:

```bash
Alice is trying to contact!
```




[img_01]: img/01.png "Input Text"
